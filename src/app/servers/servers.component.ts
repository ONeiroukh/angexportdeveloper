import { Component, OnInit } from '@angular/core';

@Component({
  // selector: '[app-servers]',
  // selector: '.app-servers',
  selector: 'app-servers',
  // template: `
  //   <app-server></app-server>
  //   <app-server></app-server>`,
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  allowNewServer = false;
  serverCreationStatus = 'No server was created!';
  serverName = 'Testserver';
  servers=["server1", "test server"];
  // constructor() {
  //   setTimeout(() => {
  //     this.allowNewServer = true;
  //   }, 0001);
  // }

constructor() {
  this.allowNewServer = true;
}

  ngOnInit() {
  }

  onCreateServer() {
    this.servers.push(this.serverName);
    this.serverCreationStatus = 'Server was created! Name is ' + this.serverName;
  }

  onUpdateServerName(event: Event) {
    this.serverName = "lkkk"; //(<HTMLInputElement>event.target).value
    //this.serverName ="serv Name";
  }
}
