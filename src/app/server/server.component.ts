import { Component } from '@angular/core';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html'
})
export class ServerComponent {
  serverId: number = 10;

  serverStatus: string = 'offline';

  constructor (){
    this.serverStatus = Math.random()>0.5 ? 'offline' : 'ononline';
  }
  getServerStatus() {
    return this.serverStatus;
  }

  getColor(){
    return (this.serverStatus=='offline'?'lightcoral':'rgb(88, 161, 119)');
  }

  getClass(){
    return (this.serverStatus);
  }

}
