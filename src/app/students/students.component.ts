import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  public students:any;

  constructor(public usersService:UsersService, ) {
  }

  ngOnInit(): void {
    this.students = [
      {firstname:'Seif eddine',lastname:'HALL', gender:'male' , note:12 },
      {firstname:'Issam',lastname:'BEN SALLEM', gender:'male' , note:14 },
      {firstname:'Jihane',lastname:'AL HAMDI', gender:'female' , note:13 },
      {firstname:'Fares',lastname:'KAMAL', gender:'male' , note:16 },
      {firstname:'Imene',lastname:'FEHMI', gender:'female' , note:14 },
    ];
    // this.usersService.getUsers('https://fakerestapi.azurewebsites.net/api/Users')
    // .subscribe(
    //     result => {
    //        this.students = result;
    //        console.log(result); // + من اجل فحص البيانات بالمتصفح

    //     },
    //     error => {
    //         console.log(error);
    //     }
    // );
  }

}
