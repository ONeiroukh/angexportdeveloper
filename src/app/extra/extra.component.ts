import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-extra',
  templateUrl: './extra.component.html',
  styleUrls: ['./extra.component.css']
})
export class ExtraComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  title ="hi extra component";
  var1=1;

  public users:any= [  // مصفوفة تتكون من اربعة كائنات الدين هم بدورهم اعضاء
    {name:'seif' , email:'seif@gmail.com' ,age:27},
    {name:'issam' , email:'issam@gmail.com' ,age:23},
    {name:'yasmine' , email:'yasmine@gmail.com' ,age:23},
    {name:'youcef' , email:'youcef@yahoo.com',age:24},
  ];

}
