import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApimainComponent } from './apimain.component';

describe('ApimainComponent', () => {
  let component: ApimainComponent;
  let fixture: ComponentFixture<ApimainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApimainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApimainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
