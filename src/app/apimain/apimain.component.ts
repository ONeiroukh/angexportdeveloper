import { Component, OnInit } from '@angular/core';

import { UserService } from  '../services/user.service';
//src\app\services\user.service.ts
//src\app\apimain\apimain.component.ts

@Component({
  selector: 'app-apimain',
  templateUrl: './apimain.component.html',
  styleUrls: ['./apimain.component.css']
})
export class ApimainComponent implements OnInit {


 title = 'app';
 public users: any;

 constructor(public usersService:UserService, ) {
}

ngOnInit() {
  this.usersService.getUsers('http://localhost/phpapi/api.php/users?transform=1')
  .subscribe(
      result => {
         let response:any = result; 
         this.users = response.users; // النتائج تعطي عدة قيم
         console.log(result); // + من خلال debugger من اجل فحص البيانات بالمتصفح

      },
      error => {
          console.log(error);
      }
  );

  
}

}