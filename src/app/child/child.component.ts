import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {

  constructor() { }
   // نقوم بتعريف المتغيرات التي يستقبلها المكون الابن من الاب عن طريق اضافة  @Input() 
   @Input() my_website: string;
   
   // متغيرات الابن
   var1: string;
   var2: number;
   var3: boolean;

  ngOnInit(): void {
  }

}
