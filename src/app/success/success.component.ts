import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styles: [`
  p {
    padding:20px;
    background-color:rgb(130, 209, 130);
    border: 1px solid green;
  }`]
})
export class SuccessComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
