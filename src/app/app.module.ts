import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { ServerComponent } from './server/server.component';
import { ServersComponent } from './servers/servers.component';
import { ExtraComponent } from './extra/extra.component';
import { HeaderComponent } from './pageParts/header/header.component';
import { SliderComponent } from './pageParts/slider/slider.component';
import { FooterComponent } from './pageParts/footer/footer.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { AppRoutingModule } from './app-routing.module';
import { StudentsComponent } from './students/students.component';
import { MainComponent } from './main/main.component';
import { IndexComponent } from './index/index.component';

import { UsersService } from './users.service';
import { StudentsdataComponent } from './studentsdata/studentsdata.component';
import { HttpClientModule } from '@angular/common/http';
import { TruncatePipe } from './truncate.pipe';
import { ParentComponent } from './parent/parent.component';
import { ChildComponent } from './child/child.component';
import { FormsComponent } from './forms/forms.component'; // + here

import { ReactiveFormsModule } from '@angular/forms';
import { ApimainComponent } from './apimain/apimain.component';

import { UserService } from './services/user.service';
import { EditComponent } from './edit/edit.component'; // +

@NgModule({
  declarations: [
    AppComponent,
    ServerComponent,
    ServersComponent,
    ExtraComponent,
    HeaderComponent,
    SliderComponent,
    FooterComponent,
    AboutusComponent,
    StudentsComponent,
    MainComponent,
    IndexComponent,
    StudentsdataComponent,
    TruncatePipe,
    ParentComponent,
    ChildComponent,
    FormsComponent,
    ApimainComponent,
    EditComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule, // + here
    ReactiveFormsModule,
  ],
  providers: [UsersService,
    UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
