import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'; // + اضف



@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.css']
})


export class FormsComponent implements OnInit {

 
  ngOnInit(): void {
  }

  public  title = 'Angular form';

// معرف الفورم
  public _loginForm:FormGroup;
//مقبض يحدد حدث الضغط على زر التأكيد
  public _submitted:boolean = false;

   


  constructor( public _formBuilder:FormBuilder,) 
  
  {
    this.createForm();
  }

  public createForm(){
  this._loginForm = this._formBuilder.group({
// قواعد النمودج تحدد عن طريق الحقل ثم القاعدة
        username: ['', Validators.required],
        password: ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      });
  }

//دالة ارسالة البيانات 
  onSubmit(elementValues: any) {
    this._submitted = true;
    alert('Username is : '+elementValues.username +' - Password is : '+elementValues.password );
    }

}


