import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';  //+
import { map } from 'rxjs/operators';  //+

@Injectable({
  providedIn: 'root'
})
export class UserdataService {

  constructor(public httpClient: HttpClient,) { }  //+


  getUsers(url:string){  //+
   return  this.httpClient.get(url)
      .pipe(
        map(res => res) 
      );
  }

}