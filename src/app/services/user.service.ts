import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  
  constructor(public httpClient: HttpClient,) { }  //+


  getUsers(url:string){  //+
   return  this.httpClient.get(url)
      .pipe(
        map(res => res) 
      );
  }

  getUser(url:string){  // http://localhost/webapi/api.php/users/1?transform=1 مثال عن المسار
    return  this.httpClient.get(url)
       .pipe(
         map(res => res) 
       );
   }


   public update(url:any , data:any) {  
    return this.httpClient
        .put(url , data )                            
        .pipe(
            map((response) => {
                  return response;       
            })
        );
  }


}
