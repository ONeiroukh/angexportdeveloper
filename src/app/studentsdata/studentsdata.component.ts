import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';

//src\app\users.service.ts
//src\app\studentsdata\studentsdata.component.ts


@Component({
  selector: 'app-studentsdata',
  templateUrl: './studentsdata.component.html',
  styleUrls: ['./studentsdata.component.css']
})
export class StudentsdataComponent implements OnInit {


  public studentsdata:any;

  constructor(public usersService:UsersService, ) {
  }

  ngOnInit(): void {

    // UsersService.getUsers('https://fakerestapi.azurewebsites.net/api/Users')
    // .subscribe(
    //     result => {
    //        this.Studentsdata = result;
    //        console.log(result); // + من اجل فحص البيانات بالمتصفح

    //     },
    //     error => {
    //         console.log(error);
    //     }
    // );

    this.usersService.getUsers('https://fakerestapi.azurewebsites.net/api/Users')
    .subscribe(
        result => {
           this.studentsdata = result;
           //console.log(result); // + من اجل فحص البيانات بالمتصفح

        },
        error => {
            console.log(error);
        }
    );
  }
  


}
