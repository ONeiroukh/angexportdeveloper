import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsdataComponent } from './studentsdata.component';

describe('StudentsdataComponent', () => {
  let component: StudentsdataComponent;
  let fixture: ComponentFixture<StudentsdataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsdataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsdataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
