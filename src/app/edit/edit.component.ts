import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'; 
import { UserService } from '../services/user.service'; 
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  public  title = 'Update User';

  public _updateForm:FormGroup;
  public _submitted:boolean = false;
  public user:any;
  public _errorMessage:any;
  
  constructor(
      public _formBuilder:FormBuilder,
      public userService:UserService,   
      protected activatedRoute:ActivatedRoute,
    ) {
     this.createForm();
  }


  ngOnInit() { 
      this.activatedRoute.params.subscribe(params => {                  
          //  update case
          if(typeof params['id'] !== "undefined") {       
            let id = Number.parseInt(params['id']);
            this.userService.getUser("http://localhost/phpapi/api.php/users/"+id+"?transform=1")
                .subscribe(
                  response  => {
                    this.user = response; 
                    console.log(this.user); 
                  },
                    error => {
                      this._errorMessage = error.data.message;
                    }
                );
          }
      });
  }
  

 


  public createForm(){
    this._updateForm = this._formBuilder.group({
      username: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
    });
}

onSubmit(elementValues: any) {  // +  أصف الدالة 
  let id:number = this.user.id;
  this._submitted = false;
  let url ="http://localhost/phpapi/api.php/users/"+id;
  let data:any = {                  
          "username": elementValues.username,
          "password": elementValues.password,
          "email": elementValues.email,
  };

this.userService.update(url,data)
    .subscribe(
        result => {
          this._submitted = true;
          this.user = data;
          alert('success : User Updated !');
        },
        error => {
          this._submitted = true;
          this._errorMessage = error;
        }
    );
}

}