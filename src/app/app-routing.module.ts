
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutusComponent } from './aboutus/aboutus.component';
import { StudentsComponent } from './students/students.component'; // +
import { MainComponent } from './main/main.component'; // +
import { IndexComponent } from './index/index.component'; // +
import { StudentsdataComponent } from './studentsdata/studentsdata.component'; // +
import { FormsComponent } from './forms/forms.component'; // +
import { ApimainComponent } from './apimain/apimain.component'; // +
import { EditComponent } from './edit/edit.component';


const routes: Routes = [

    {path: 'about',component: AboutusComponent,} ,
    {path: 'students',component: StudentsComponent,} ,
    {path: 'main',component: MainComponent,} ,
    {path: 'serve',component: StudentsdataComponent,} ,
    {path: 'forms',component: FormsComponent,} ,
    {path: 'apimain',component: ApimainComponent,} ,
    {path: 'user/edit/:id',component: EditComponent,} ,  //  لاحظ أن هدا المسار يحتاج لتمرير   id+
    
    
    //{path: '/',component: IndexComponent,} ,

    
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }


